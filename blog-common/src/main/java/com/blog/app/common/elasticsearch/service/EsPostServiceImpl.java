package com.blog.app.common.elasticsearch.service;

import com.blog.app.common.elasticsearch.model.Post;
import com.blog.app.common.elasticsearch.repository.PostRepository;
import com.blog.app.common.elasticsearch.repository.PostRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EsPostServiceImpl implements EsPostService {

    private PostRepository postRepository;
    private PostRepositoryCustom postRepositoryCustom;

    @Autowired
    public EsPostServiceImpl(PostRepository postRepository, PostRepositoryCustom postRepositoryCustom) {
        this.postRepository = postRepository;
        this.postRepositoryCustom = postRepositoryCustom;
    }

    @Override
    public Post save(Post post) {
        return postRepository.save(post);
    }
    
    @Override
    public List<Post> findPostContainingText(String text) {
        return postRepositoryCustom.findPostContainingText(text);
    }
}