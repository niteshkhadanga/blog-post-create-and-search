package com.blog.app.common.elasticsearch.service;

import com.blog.app.common.elasticsearch.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EsPostService {
    Post save(Post post);
    List<Post> findPostContainingText(String text);
}