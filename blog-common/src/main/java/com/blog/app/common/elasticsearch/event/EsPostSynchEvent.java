package com.blog.app.common.elasticsearch.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.blog.app.common.elasticsearch.model.Author;

import java.util.List;
import java.util.TimeStamp;

public class EsPostSynchEvent {

    private String id;
    private String title;
    private String userId;
    private String timeStamp;
    private String blogContent;

    public EsPostSynchEvent() {
        //For Spring Web layer to bind cleanly..
    }

    @JsonCreator
    public EsPostSynchEvent(@JsonProperty("id") String id,
                            @JsonProperty("title") String title,
                            @JsonProperty("userId") String userId,
                            @JsonProperty("timeStamp") String timeStamp,
                            @JsonProperty("blogContent") String blogContent){
        this.id = id;
        this.title = title;
        this.userId = userId;
        this.timeStamp = timeStamp;
        this.blogContent = blogContent;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("title", title)
                .add("userId", userId)
                .add("timeStamp", timeStamp)
                .add("blogContent", blogContent)
                .toString();
    }
}
