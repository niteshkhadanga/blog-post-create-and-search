# Blog-Post-Create-and-Search

The project mainly consists of two APIs, first one for creating a blog post and the second one for searching for phrases in the blog posts.

The following have been used to implement the functionality of the above two APIs:

* mysql - as the primary storage for the blog entries; via jpa
* rabbitmq (as a message queue to dispatch message to elasticsearch, via spring integration)
* elasticsearch (as a secondary storage for full-text search of the blog entries)
* docker-compose - mysql, rabbitmq and elasticsearch are docker images that run via docker container

To run the blog application:

* Download Docker, and cd into /docker, then run: docker-compose up
* Run the Main Blog (Spring boot app com.blog.app.BlogAppMain)
* Run the elasticsearch collector (es.collector.EsEventCollector to pick up event from rabbitmq)

