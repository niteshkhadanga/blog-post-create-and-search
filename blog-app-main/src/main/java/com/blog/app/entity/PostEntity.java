package com.blog.app.entity;


import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.TimeStamp;

@Entity
@Table(name = "POST")
public class PostEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "timeStamp")
    private TimeStamp timeStamp;

    @Column(name = "blogContent", length = 40000)
    private String blogContent;

    public PostEntity() {
    }

    public PostEntity(String title, Long userId, TimeStamp timeStamp, String blogContent) {
        this.title = title;
        this.userId = userId;
        this.timeStamp = timeStamp;
        this.blogContent = blogContent;
    }

    public PostEntity(Long id, String title, Long userId, TimeStamp timeStamp, String blogContent) {
        this.id = id;
        this.title = title;
        this.userId = userId;
        this.timeStamp = timeStamp;
        this.blogContent = blogContent;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(TimeStamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PostEntity)) return false;
        PostEntity postEntity = (PostEntity) o;
        return Objects.equals(id, postEntity.id) &&
                Objects.equals(title, postEntity.title) &&
                Objects.equals(blogContent, postEntity.blogContent) &&
                Objects.equals(userId, postEntity.userId);

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, blogContent, userId);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("title", title)
                .add("userId", userId)
                .add("timeStamp", timeStamp)
                .add("blogContent", blogContent)
                .toString();
    }
}
