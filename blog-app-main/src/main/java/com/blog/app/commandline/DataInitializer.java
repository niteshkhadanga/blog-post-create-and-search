package com.blog.app.commandline;

import com.blog.app.entity.PostEntity;
import com.blog.app.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import static java.util.Arrays.*;

@Component
public class DataInitializer implements CommandLineRunner {

    @Autowired
    PostService postService;


    @Override
    public void run(String... strings) throws Exception {
        //createBlog().stream().forEach(a -> postService.save(a));
    }

    private List<PostEntity> createBlog() {
        PostEntity postEntity1 = new PostEntity();
        postEntity1.setTitle("Blog Post 1");
        postEntity1.setUserId("001");
        postEntity1.setTimeStamp(new Timestamp(date.getTime()));
       	postEntity1.setBlogContent("I've always had a thing against Landon Donovan. The tag of 'best US soccer player of his generation' rang a bit hollow when you looked at what he's achieved on a global scale. His two aborted attempts at Bayer Leverkusen in Germany really frustrated me.

You see, the guy has undeniable skill. He might not be considered world class per se, but he definitely is the best player in the MLS pre-Beckham by a country mile. If he had stuck with it and tried to develop himself in Europe at an earlier age, I believe he would've achieved much more and became a far better player than he is today.

What I find really irritating wasn't that he had tried to play at a club and failed, it was the manner of his failure. It wasn't based on not being good enough to get into the first team. It wasn't about a bad atmosphere or a league that was too difficult for him. It was his inability or unwillingness to experience life outside of the US. His first stint in Germany was at a very young age (16, I think), and he struggled to adapt to life in Europe and so he was allowed to go back on loan to the MLS until he matured enough to be able to handle the situation. In San Jose he prospered, and was consistently the best performer on a mediocre team. He publicly spoke about his nightmare in Germany and not being able to cope with the move away from the US.

Then came the second stint at the age of 23. Bayer Leverkusen wanted a player of his calibre back to contribute, and he was forced to leave the US again. Once again, he failed to adjust to his surroundings and a move back to the MLS and the LA Galaxy followed suit. I was really disappointed to hear about it at the time, since I thought that surely Germany cannot be THAT bad? And even if its not exactly where he'd like to live, it is more than simply a positive career step and going back the mediocrity of the MLS was the least ambitious thing I have ever heard of.

My friends struggled to see why I was so upset. Surely he had a right to choose where he wanted to live? My grief was in the fact that he was living my dream and making a mess of it. He had the talent to be a world class player, he was a professional footballer with everything he ever wanted.. and he couldn't leave the comfort zone of 'America, the Best Place on Earth'.

Landon came out recently and said that he is now dreaming of a move to Spain or England. He recognises that his game would improve drastically over in Europe, but he's unsure now whether any club would sign him after his antics in Germany. He's finally matured enough to think with a professional mindset.. but he's almost 27 now, and a lot of time was lost where he could've further honed his craft in more competitive surroundings.

As a person, I don't know Mr. Donovan and have little regard to how his life turns out to be. Its the concept that bothers me, the fact that his reluctance to put some pressure on himself to further improve on his God given talent stems from his inability to leave the US cocoon. When I first went to university, my roommate who hails from New York asked me why anyone would want to live anywhere but the United States. His lack of geographical orientation astounded me, and I wondered sometimes how many Americans can locate any given country on a map of Europe if presented with the challenge. I'm sure there are many worldly and knowledgable yanks, but the majority seem resistant to the fact that the world holds much more than just the 50 states. My roommate went on to developing a fondness for Europe and a willingness to travel around the world, despite his initial reluctance. It took him 6 years, but he came 'round in the end.. as did all the other Americans who I'm proud to say took a piece of Europe back home with them. May it extend to affect all of the people surrounding them.

So, Landon, I'm glad that you've finally had your moment of enlightenment. There's more out there than simply the good ol' US of A and the MLS. Time to see if firsthand.");


       	PostEntity postEntity2 = new PostEntity();
        postEntity2.setTitle("Blog Post 2");
        postEntity2.setUserId("002");
        postEntity2.setTimeStamp(new Timestamp(date.getTime()));
       	postEntity2.setBlogContent("Been back in Bahrain for a while now, just started working. Feels good to be doktoring around after a long layoff, but the routine of having to wake up in the mornings again will need some getting used to. Adjusted to living here by now, so I guess Ireland is mostly out of my system at this point. There are some things you have to accept as they are around these parts, even though they could do with some (read: a lot of) improvement.

Was driving on the highway the other day and something peculiar happened to me. Not just once, but three times within the span of two weeks. I would be driving at a reasonable speed (about 110 km/hr) in the fast lane and a speed demon would show up behind me, all guns blazing. In most cases I would move over to the right and allow him/her (because women are just as bad these days) to pass. In all three cases there were cars in the middle lane and so I had to carry on until I got a chance to switch lanes. Instead of being polite enough to wait until I moved over, the drivers in this case would accelerate and overtake me on the hard shoulder, squeezing between my car and the barrier to the left at around 160km/hr. I could not believe what I was seeing. Speeding and high-risk driving are common enough, but this is just suicidial. Needless to say, it brought out a bit of road rage and I guess honking the horn and flashing my headlights were the most I could do in my frustration at those fucking idiots.

This ties into another phenomenon I have been witnessing. At times during the night, around 11pm and onwards, people would choose to ignore red lights located around the kingdom. They would slow down, see that the intersection was not busy, and would just drive off like nothing happened. It drives me insane seeing this, and even though they might justify it by saying that they've slowed down and saw no other cars coming.. there simply is no jusitfication.

All of these things, aside from demonstrating that a large proportion of bahrainis have completely lost any iota of driving etiquette left within them, is showing us the unfortunate mentality that most of them live with. This idea that they are in some way superior to those around them, that their time is worth more and that they can bend or break the law according to their wishes is simply laughable. No one is above the law, and I don't care how busy or in a rush you are, endangering other people's lives in such a flippant fashion is simply unacceptable. You almost wish you could say this to their faces, but something tells me that anyone who would do something like that without feeling any remorse is not worth talking to in the first place. Why have we turned into such a materialistic, consumerist society that is defined by the selfishness of the individual and not the love of the collective? Everyone is so concerned with how to progress themselves that they are openly willing to step (and shit) on eveyone else's heads. It a terribly sad state of affairs to find ourselves in, and I hope that something drastic happens in the near future so that we may shake off this frankly primitive way of living.
");

        return asList(postEntity1, postEntity2);
    }
}
