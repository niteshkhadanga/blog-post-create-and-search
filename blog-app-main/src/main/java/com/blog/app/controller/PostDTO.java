package com.blog.app.controller;

import java.sql.Timestamp;


public class PostDTO {
    private Long id;
    private Long userId;
    private String title;
    private Timestamp timeStamp;
    private String blogContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setTag(String blogContent) {
        this.blogContent = blogContent;
    }

}
